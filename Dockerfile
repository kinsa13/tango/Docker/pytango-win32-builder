# escape=`

FROM mcr.microsoft.com/windows/servercore:ltsc2019 as base

RUN powershell -NoProfile -InputFormat None -ExecutionPolicy Bypass `
        -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && `
    SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"

FROM base as builder

RUN powershell -NoProfile -InputFormat None `
        -Command "choco install visualcpp-build-tools --version 15.0.26228.20170424 -y; `
            Write-Host 'Waiting for Visual C++ Build Tools to finish'; `
            Wait-Process -Name vs_installer"

RUN choco install git 7zip cyg-get vcredist140 -y && `
    choco install --forceX86  python3 --version=3.9.10 -y && ` 
    cyg-get make patch

ENV PROJDIR=C:\projects `
    PYTHONPATH=c:/Python39

WORKDIR "C:/Program Files (x86)/Microsoft Visual Studio/2017/BuildTools/VC/Auxiliary/Build/"

RUN md %PROJDIR%

ARG OMNI_VER=4.2.4
ENV SRCDIR=$PROJDIR\omniORB_src\src `
    DEST_DIR=$PROJDIR\dist_omni

ADD set_path.bat $PROJDIR

RUN vcvarsall x86 -vcvars_ver=14.16.27023 && `
    cd %PROJDIR% && md omniORB_src && `
    # hack to set PATH environment `
    set_path.bat && `
    curl -L https://sourceforge.net/projects/omniorb/files/omniORB/omniORB-%OMNI_VER%/omniORB-%OMNI_VER%.tar.bz2/download -o %PROJDIR%\omniORB.tar.bz2 && `
    7z x omniORB.tar.bz2 -so | tar xf - -C omniORB_src  --strip-components=1  && `
    cd %SRCDIR% && `
    sed -i "s/^#platform = x86_win32_vs_15/platform = x86_win32_vs_15/g" ..\config\config.mk && `
    sed -i "s-^#PYTHON = /cygdrive/c/Python36/python-PYTHON = /cygdrive/c/Python39/python-g" ..\mk\platforms\x86_win32_vs_15.mk && `
    make -j4 export && `
    xcopy %SRCDIR%\..\bin %DEST_DIR%\bin /E/Y/I && `
    xcopy %SRCDIR%\..\lib %DEST_DIR%\lib /E/Y/I && `
    xcopy %SRCDIR%\..\include %DEST_DIR%\include /E/Y/I && `
    cd %PROJDIR% && rd /s/q omniORB_src && del omniORB.tar.bz2 set_path.bat

ARG TANGO_IDL_VER=main
ENV SRCDIR=$PROJDIR\idl_src `
    DEST_DIR=$PROJDIR\dist_idl

RUN vcvarsall x86 -vcvars_ver=14.16.27023 && `
    git clone --depth 1 -b %TANGO_IDL_VER% https://gitlab.com/tango-controls/tango-idl.git %SRCDIR% && `
    md %SRCDIR%\build && cd %SRCDIR%\build && `
    cmake -DCMAKE_INSTALL_PREFIX=%DEST_DIR% .. && `
    cmake --build . --config Release --target install && `
    cd %PROJDIR% && rd /s/q %SRCDIR%

ENV DEST_DIR=$PROJDIR\dist_pthreads

RUN cd %PROJDIR% && `
    curl -L https://github.com/tango-controls/Pthread_WIN32/releases/download/2.9.1/pthreads-win32-2.9.1_win32-msvc15.zip -o pthreads.zip && `
    7z x pthreads.zip -o%DEST_DIR% && del pthreads.zip
    
ARG ZMQ_VER=v4.3.4
ENV SRCDIR=$PROJDIR\libzmq_src `
    DEST_DIR=$PROJDIR\dist_zmq

RUN vcvarsall x86 -vcvars_ver=14.16.27023 && `
    git clone -b %ZMQ_VER% --depth 1 https://github.com/zeromq/libzmq %SRCDIR% && `
    md %SRCDIR%\build && cd %SRCDIR%\build && `
    cmake -DCMAKE_INSTALL_PREFIX=%DEST_DIR% -DZMQ_BUILD_TESTS=0 -DENABLE_CLANG=0 -DBUILD_TESTS=0 `
        -DENABLE_RADIX_TREE=0 -DENABLE_DRAFTS=0 -DENABLE_CURVE=0 -DENABLE_CPACK=0 -DENABLE_WS=0 `
        -DWITH_DOCS=0 -DENABLE_PRECOMPILED=0 -DCMAKE_CXX_FLAGS_RELEASE="/MT /O2 /Ob2 /DNDEBUG" `
        -DCMAKE_C_FLAGS_RELEASE="/MT /O2 /Ob2 /DNDEBUG" -DCMAKE_BUILD_TYPE=Release `
        -DBUILD_SHARED=0 -G"Ninja" %SRCDIR% && `
    ninja install && `
    cd %PROJDIR% && rd /s/q %SRCDIR%

ARG CPPZMQ_VER=v4.8.1
ENV SRCDIR=$PROJDIR\cppzmq_src

RUN vcvarsall x86 -vcvars_ver=14.16.27023 && `
    git clone -b %CPPZMQ_VER% --depth 1 https://github.com/zeromq/cppzmq.git %SRCDIR% && `
    md %SRCDIR%\build && cd %SRCDIR%\build && `
    cmake -DCPPZMQ_BUILD_TESTS=OFF -DCMAKE_INSTALL_PREFIX=%DEST_DIR% .. && `
    cmake --build . --config Release --target install && `
    cd %PROJDIR% && rd /s/q %SRCDIR%

ARG CPPTANGO_VER=9.3.5
ENV SRCDIR=$PROJDIR\cpptango_src `
    DEST_DIR=c:/projects/dist `
    SED=C:\tools\cygwin\bin\sed

RUN vcvarsall x86 -vcvars_ver=14.16.27023 && `
    git clone -b %CPPTANGO_VER% --depth 1 https://gitlab.com/tango-controls/cpptango.git %SRCDIR% && `
    md %SRCDIR%\build && cd %SRCDIR%\build && `
    # update omniORB lib versions `
    %SED% -i -e "s/421_/424_/g" -e "s/40_/41_/g" `
             -e "s/bin\/Release/bin/g" -e "s/lib\/Release/lib/g" `
             -e "s/-v141-/-/g" -e "s/4_0_5/4_3_4/g" `
             -e "/.*-mt-4.*/d" `
          ..\configure\cmake_win.cmake && `
    %SED% -i -e "s/-v141-/-/g" -e "s/4_0_5/4_3_4/g" `
             -e "s/bin\/Release/bin/g" -e "s/lib\/Release/lib/g" `
          ..\configure\CMakeLists.txt && `
    cmake -DCMAKE_INSTALL_PREFIX=%DEST_DIR% -DBUILD_SHARED_LIBS=OFF `
          -DCMAKE_CXX_FLAGS_RELEASE="/MT" `
          -DIDL_BASE="%DEST_DIR%_idl" -DOMNI_BASE="%DEST_DIR%_omni" `
          -DZMQ_BASE="%DEST_DIR%_zmq" -DCPPZMQ_BASE="%DEST_DIR%_zmq" `
          -DPTHREAD_WIN="%DEST_DIR%_pthreads" `
          -DBUILD_TESTING=OFF -DTANGO_INSTALL_DEPENDENCIES=ON `
          .. && `
    cmake --build ./ --target install --config Release -j && `
    cd %PROJDIR% && rd /s/q %SRCDIR%

ARG BOOST_VER=1.74.0
ENV SRCDIR=$PROJDIR\boost_src `
    DEST_DIR=$PROJDIR\dist_boost

RUN vcvarsall x86 -vcvars_ver=14.16.27023 && `
    cd %PROJDIR% && md %SRCDIR% && `
    curl -L https://boostorg.jfrog.io/artifactory/main/release/1.74.0/source/boost_1_74_0.tar.bz2 -o %PROJDIR%\boost.tar.bz2 && `
    7z x boost.tar.bz2 -so | tar xf - -C %SRCDIR%  --strip-components=1  && `
    cd %SRCDIR% && `
    bootstrap vc141 && `
    b2 --with-python --prefix="%DEST_DIR%" address-model=32 variant=release link=static threading=multi runtime-link=static install && `
    cd %PROJDIR% && rd /s/q %SRCDIR% && del boost.tar.bz2

ARG PYTANGO_VER=v9.3.3 `
    PYTHON_VER=39 `
    PYTHON_ROOT=c:/Python39
ENV SRCDIR=$PROJDIR\pytango_src `
    BOOST_ROOT=$PROJDIR\dist_boost `
    TANGO_ROOT=$PROJDIR\dist

ADD pytango.patch $PROJDIR

RUN vcvarsall x86 -vcvars_ver=14.16.27023 && `
    pip install nose wheel numpy && `
    git clone --depth=1 --branch %PYTANGO_VER% https://gitlab.com/tango-controls/pytango %SRCDIR% && `
    cd %SRCDIR% && git apply %PROJDIR%\pytango.patch && `
    md build && cd build && `
    cmake .. -G"Ninja" -DCMAKE_CXX_FLAGS_RELEASE="/MT /O2 /Ob2 /DNDEBUG" -DCMAKE_BUILD_TYPE="Release" && `
    ninja

FROM base

RUN choco install vcredist140 -y && `
    choco install --forceX86  python3 --version=3.9.10 -y

COPY --from=builder c:/projects/pytango_src/dist c:/dist
COPY --from=builder c:/projects/pytango_src/tests c:/tests
ADD https://github.com/spirit1317/tango-test-exe/raw/main/TangoTest.exe c:/tests
ADD runtests.bat c:/

RUN cd c:\dist && `
    for /r %a in (*.whl) do pip install %~nxa

CMD xcopy \dist \dest /e/y/i
