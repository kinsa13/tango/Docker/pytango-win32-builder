# Dockerfile for building the win32 version of [PyTango](https://gitlab.com/tango-controls/pytango)

To build:
```
docker build -t <tag name> .
```

To run tests:
```
docker run -it --rm <tag name> c:\runtests.bat
```

To extract output:
```
docker run -it --rm -v %cd%:C:/dest <tag name>
```
